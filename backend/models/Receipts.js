const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReceiptSchema = new Schema({
	schedule: [],
	packName: String,
	totalCost: String
});

module.exports = mongoose.model('Receipts', ReceiptSchema);
