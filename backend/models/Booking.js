const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const BookingSchema = new Schema({
	bookingCode: String,
	status: {
		type: String,
		default: 'Received'
	},
	schedule: [],
	logs: [
		{
			status: {
				type: String,
				default: 'Received'
			},
			updateDate: {
				type: String,
				default: moment(new Date()).format('MMM/DD/YY')
			},
			message: String
		}
	],
	cost: Number,
	name: String,
	email: String,
	sitterName: String,
	sitterId: String,
	packName: String,
	packId: String
});

module.exports = mongoose.model('Booking', BookingSchema);
