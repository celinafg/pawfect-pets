const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SitterSchema = new Schema({
	sitterName: String,
	isAdmin: {
		type: Boolean,
		default: false
	},
	description: String,
	status: {
		type: String,
		default: 'Available'
	}
});

module.exports = mongoose.model('Sitter', SitterSchema);
