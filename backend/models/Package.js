const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const PackageSchema = new Schema({
	code: String,
	name: String,
	email: String, 
	status: {
		type: String,
		default: 'Received'
	},
	description: String,
	requestDate: {
		type: String,
		default: moment(new Date()).format('MMM/DD/YY')
	},
	cost: Number
});

module.exports = mongoose.model('Package', PackageSchema);
