const express = require('express');
const app = express();
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');
const stripe = require('stripe')('sk_test_zaBPxW1zn6Ygjd5MpLEuwpye00bH1U5BrX');

// const multer = require('multer');

// let storage = multer.diskStorage({
// 	destination: (req, file, cb) => {
// 		cb(null, 'public/images/uploads');
// 	},
// 	filename: (req, file, cb) => {
// 		cb(null, Date.now() + '-' + file.originalname);
// 	}
// });

// const upload = multer({ storage });
// const path = require('path');
// app.use(express.static(path.join(__dirname, 'public')));

mongoose
	.connect('mongodb+srv://admin:adminadmin@day1mongo-04tfw.mongodb.net/test?retryWrites=true&w=majority', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true
	})
	.then(() => {
		console.log('remote database connection established');
	});

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

app.listen(config.port, () => {
	console.log(`listening on port ${config.port}`);
});

//packages api
const packages = require('./routes/packages_router');
app.use('/', packages);

//users api
const users = require('./routes/users_router');
app.use('/', users);

//auth api
const auth = require('./routes/auth_router');
app.use('/', auth);

//appointments api
const bookings = require('./routes/bookings_router');
app.use('/', bookings);

//sitter api
const sitters = require('./routes/sitters_router');
app.use('/', sitters);

// const auth = require('./routes/auth_router');
// app.use('/', auth);

// app.post('/charge', async (req, res) => {
// 	try {
// 		stripe.customers
// 			.create({
// 				email: req.body.email,
// 				customerName: req.body.customerName,
// 				source: 'tok_visa'
// 			})
// 			.then((customers) => {
// 				stripe.charges.create({
// 					amount: req.body.totalPayment,
// 					currency: 'php',
// 					source: 'tok_visa',
// 					description: 'please work'
// 				});
// 			});
// 	} catch (e) {
// 		console.log(e);
// 		res.status(500).end();
// 	}
// });

app.post('/charge', (req, res) => {
	console.log(req.body);
	try {
		stripe.customers
			.create({
				email: req.body.email,
				description: req.body.description,
				source: 'tok_visa'
			})
			.then((customers) =>
				stripe.charges.create({
					amount: req.body.amount,
					currency: 'php',
					source: 'tok_visa',
					description: req.body.amount,
					receipt_email: req.body.email
				})
			)
			.then((result) => {
				res.send(result);
				console.log(result);
			});
	} catch (e) {
		console.log(e);
	}
});
