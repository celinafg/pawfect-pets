const express = require('express');
const UserRouter = express.Router();
const UserModel = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth_middleware');

UserRouter.post('/register', async (req, res) => {
	if (!req.body.email) return res.status(400).send('Email is Required');
	if (!req.body.password) return res.status(400).send('Password is Requirede');

	let user = UserModel({
		name: req.body.name,
		email: req.body.email
	});

	let salt = bcrypt.genSaltSync(10);
	let hashed = bcrypt.hashSync(req.body.password, salt);
	user.password = hashed;

	try {
		user = await user.save();
		res.send(user);
	} catch (e) {
		res.status(400).send('Invalid Data');
	}
});

//show assets
UserRouter.get('/showusers', async (req, res) => {
	try {
		let users = await UserModel.find();
		res.send(users);
	} catch (e) {
		console.log(e);
	}
});

UserRouter.patch('/updateadminstatus/:id', auth, async (req, res) => {
	try {
		let condition = { _id: req.params.id };
		let update = { isAdmin: req.body.isAdmin };
		let updatedUser = await UserModel.findByIdAndUpdate(condition, update, { new: true });

		res.send(updatedUser);
	} catch (e) {
		res.status(400).send('error');
	}
});

module.exports = UserRouter;
