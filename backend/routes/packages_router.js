const express = require('express');
const PackageRouter = express.Router();
const PackageModel = require('../models/Package');

//add packages
PackageRouter.post('/addpackage', async (req, res) => {
	try {
		let package = PackageModel({
			code: req.body.code,
			name: req.body.name,
			description: req.body.description,
			cost: req.body.cost,
			logs: [ { message: 'hello' } ]
		});

		package = await package.save();

		res.send(package);
	} catch (e) {
		console.log(e);
	}
});

// show packages
PackageRouter.get('/showpackages', async (req, res) => {
	try {
		let packages = await PackageModel.find();
		res.send(packages);
	} catch (e) {
		console.log(e);
	}
});

// show package by id
// PackageRouter.get('/showpackagebyid/:id', async (req, res) => {
// 	try {
// 		let package = await PackageModel.findById(req.params.id);
// 		res.send(package);
// 	} catch (e) {
// 		console.log(e);
// 	}
// });

//delete package
PackageRouter.delete('/deletepackage/:id', async (req, res) => {
	try {
		let deletedPackage = await PackageModel.findByIdAndDelete(req.params.id);
		res.send(deletedPackage);
	} catch (e) {
		console.log(e);
	}
});

//update asset by id
// AssetRouter.put('/updateasset/:id', async (req, res) => {
// 	try {
// 		let asset = await AssetModel.findById(req.params.id);
// 		if (!asset) {
// 			return res.status(404).send(`asset can't be found`);
// 		}
// 		let condition = { _id: req.params.id };
// 		let updates = {
// 			name: req.body.name,
// 			description: req.body.description,
// 			serialNumber: req.body.serialNumber,
// 			category: req.body.category,
// 			stock: req.body.stock
// 		};

// 		let updatedAsset = await AssetModel.findOneAndUpdate(condition, updates, { new: true });
// 		res.send(updatedAsset);
// 	} catch (e) {
// 		console.log(e);
// 	}
// });

// AssetRouter.patch('/updatestock/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {stock: req.body.stock};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatecategory/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {category: req.body.category};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatedescription/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {description: req.body.description};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatename/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {name: req.body.name};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

module.exports = PackageRouter;
