const express = require('express');
const SitterRouter = express.Router();
const SitterModel = require('../models/Sitter');

//add appointments
SitterRouter.post('/addsitter', async (req, res) => {
	try {
		let sitter = SitterModel({
			sitterName: req.body.sitterName,
			description: req.body.description
		});

		sitter = await sitter.save();

		res.send(sitter);
	} catch (e) {
		console.log(e);
	}
});

// show packages
SitterRouter.get('/showsitters', async (req, res) => {
	try {
		let sitters = await SitterModel.find();
		res.send(sitters);
	} catch (e) {
		console.log(e);
	}
});

// show package by id
SitterRouter.get('/showsitter/:id', async (req, res) => {
	try {
		let sitter = await SitterModel.findById(req.params.id);
		res.send(sitter);
	} catch (e) {
		console.log(e);
	}
});

//delete package
SitterRouter.delete('/deletesitter/:id', async (req, res) => {
	try {
		let deletedSitter = await SitterModel.findByIdAndDelete(req.params.id);
		res.send(deletedSitter);
	} catch (e) {
		console.log(e);
	}
});

//update asset by id
// AssetRouter.put('/updateasset/:id', async (req, res) => {
// 	try {
// 		let asset = await AssetModel.findById(req.params.id);
// 		if (!asset) {
// 			return res.status(404).send(`asset can't be found`);
// 		}
// 		let condition = { _id: req.params.id };
// 		let updates = {
// 			name: req.body.name,
// 			description: req.body.description,
// 			serialNumber: req.body.serialNumber,
// 			category: req.body.category,
// 			stock: req.body.stock
// 		};

// 		let updatedAsset = await AssetModel.findOneAndUpdate(condition, updates, { new: true });
// 		res.send(updatedAsset);
// 	} catch (e) {
// 		console.log(e);
// 	}
// });

// AssetRouter.patch('/updatestock/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {stock: req.body.stock};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatecategory/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {category: req.body.category};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatedescription/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {description: req.body.description};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatename/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {name: req.body.name};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

module.exports = SitterRouter;
