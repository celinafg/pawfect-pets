const express = require('express');
const BookingRouter = express.Router();
const BookingModel = require('../models/Booking');
const stripe = require('stripe')('sk_test_zaBPxW1zn6Ygjd5MpLEuwpye00bH1U5BrX');

//add appointments
BookingRouter.post('/addbooking', async (req, res) => {
	try {
		let booking = BookingModel({
			bookingCode: req.body.bookingCode,
			logs: [ { message: 'booking received' } ],
			cost: req.body.cost,
			name: req.body.name,
			schedule: req.body.schedule,
			email: req.body.email,
			sitterName: req.body.sitterName,
			sitterId: req.body.sitterId,
			packName: req.body.packName,
			packId: req.body.packId
		});

		booking = await booking.save();

		res.send(booking);
	} catch (e) {
		console.log(e);
	}
});

// show bookings
BookingRouter.get('/showbookings', async (req, res) => {
	try {
		let bookings = await BookingModel.find();
		res.send(bookings);
	} catch (e) {
		console.log(e);
	}
});

// show package by id
BookingRouter.get('/showbookingbysitterid/:sitterId', async (req, res) => {
	try {
		let booking = await BookingModel.find({ sitterId: req.params.sitterId });
		res.send(booking);
	} catch (e) {
		console.log(e);
	}
});

//show bookings by userid
BookingRouter.get('/showbookingbyuser/:userId', async (req, res) => {
	try {
		let booking = await BookingModel.find({ userId: req.params.userId });
		res.send(booking);
	} catch (e) {
		console.log(e);
	}
});

delete package;
BookingRouter.delete('/deletebooking/:id', async (req, res) => {
	try {
		let deletedBooking = await BookingModel.findByIdAndDelete(req.params.id);
		res.send(deletedBooking);
	} catch (e) {
		console.log(e);
	}
});

//charge
// BookingRouter.post('/charge', async (req, res) => {
// 	try {
// 		stripe.customers
// 			.create({
// 				email: req.body.email,
// 				// customerName: req.body.customerName,
// 				description: 'thisis a payment',
// 				source: 'tok_visa'
// 			})
// 			.then((customers) => {
// 				stripe.charges.create({
// 					amount: req.body.totalPayment,
// 					currency: 'php',
// 					source: 'tok_visa',
// 					description: 'please work'
// 				});
// 			});
// 	} catch (e) {
// 		console.log(e);
// 		res.status(500).end();
// 	}
// });

//update asset by id
// AssetRouter.put('/updateasset/:id', async (req, res) => {
// 	try {
// 		let asset = await AssetModel.findById(req.params.id);
// 		if (!asset) {
// 			return res.status(404).send(`asset can't be found`);
// 		}
// 		let condition = { _id: req.params.id };
// 		let updates = {
// 			name: req.body.name,
// 			description: req.body.description,
// 			serialNumber: req.body.serialNumber,
// 			category: req.body.category,
// 			stock: req.body.stock
// 		};

// 		let updatedAsset = await AssetModel.findOneAndUpdate(condition, updates, { new: true });
// 		res.send(updatedAsset);
// 	} catch (e) {
// 		console.log(e);
// 	}
// });

// AssetRouter.patch('/updatestock/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {stock: req.body.stock};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatecategory/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {category: req.body.category};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatedescription/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {description: req.body.description};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

// AssetRouter.patch('/updatename/:id', async (req, res) => {
// 	try {
// 		let condition = {_id:req.params.id};
// 		let update = {name: req.body.name};
// 		let updated = await AssetModel.findOneAndUpdate(condition, update, {new:true});
// 		res.send(updated)
// 	}catch(e){
// 		console.log(e)
// 	}
// })

module.exports = BookingRouter;
