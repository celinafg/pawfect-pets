import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

const loading = () => {
	return (
		<div className="bg-warning d-flex justify-content-center align-items-center">
			<h1>Loading</h1>
		</div>
	);
};

const Register = React.lazy(() => import('./views/pages/Register'));
const Login = React.lazy(() => import('./views/pages/Login'));
const Packages = React.lazy(() => import('./views/Package/Packages'));
const Sitters = React.lazy(() => import('./views/Sitter/Sitters'));
const BookingForm = React.lazy(() => import('./views/Package/components/BookingForm'));
const Payments = React.lazy(() => import('./views/Payments/Payments'));
const Home = React.lazy(() => import('./views/Layouts/Home'));
const AllBookings = React.lazy(() => import('./views/Booking/AllBookings'));

const App = () => {
	return (
		<HashRouter>
			<React.Suspense fallback={loading}>
				<Switch>
					<Route path="/" exact name="Home" render={(props) => <Home {...props} />} />
					<Route path="/register" exact name="Register" render={(props) => <Register {...props} />} />
					<Route path="/login" exact name="Login" render={(props) => <Login {...props} />} />
					<Route path="/packages" exact name="Packages" render={(props) => <Packages {...props} />} />
					<Route path="/sitters" exact name="Sitters" render={(props) => <Sitters {...props} />} />
					<Route
						path="/bookingform"
						exact
						name="BookingForm"
						render={(props) => <BookingForm {...props} />}
					/>
					<Route path="/stripe" exact name="Payments" render={(props) => <Payments {...props} />} />
					<Route
						path="/allbookings"
						exact
						name="AllBookings"
						render={(props) => <AllBookings {...props} />}
					/>{' '}
				</Switch>
			</React.Suspense>
		</HashRouter>
	);
};
export default App;
