import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

const FormInput = ({ onBlur, label, name, type, placeholder, onChange, defaultValue, required, ...props }) => {
	return (
		<>
			<FormGroup>
				<Label>{label}</Label>
				<Input
					name={name}
					type={type}
					placeholder={placeholder}
					onChange={onChange}
					defaultValue={defaultValue}
					style={required ? { border: 'solid 1px red' } : null}
				/>
			</FormGroup>
		</>
	);
};

export default FormInput;
