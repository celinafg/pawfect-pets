import React from "react";
import { Modal, ModalBody, ModalHeader, Button } from "reactstrap";
import { FormInput } from "../../../components";

const PackageForm = props => {
  return (
    <React.Fragment>
      <Modal isOpen={props.showForm} toggle={props.handleShowForm}>
        <ModalHeader
          toggle={props.handleShowForm}
          style={{ backgroundColor: "lightsalmon", color: "white" }}
        >
          Add Request
        </ModalHeader>
        <ModalBody>
          {/* <AssetInput handleAssetInput={props.handleAssetInput} assetName={props.assetName} /> */}
        </ModalBody>
        <FormInput
          label={"Sitter Name"}
          type={"text"}
          name={"sitterName"}
          placeholder="Sitter Name"
          onChange={e => [props.handleNameInput(e)]}
        />
        <FormInput
          label={"Sitter Description "}
          type={"text"}
          name={"description"}
          placeholder="Short description here"
          onChange={e => [props.handleDescription(e)]}
        />
        <Button
          color="info"
          block
          // disabled={props.assetName === '' || props.quantity <= 0 ? true : false}
          onClick={props.handleSaveSitter}
        >
          AddPackage
        </Button>
      </Modal>
    </React.Fragment>
  );
};

export default PackageForm;
