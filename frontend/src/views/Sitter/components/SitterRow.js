import React from "react";
import { Button } from "reactstrap";
// import { FormInput } from "../../../components";
import ReactHTMLTableToExcel from "react-html-table-to-excel";

const SitterRow = ({ sitters, ...props }) => {
  //   const [showLogs, setShowLogs] = useState(false);
  //   const [dropdownOpen, setDropdownOpen] = useState(false);

  //   const toggle = () => setDropdownOpen(!dropdownOpen);

  //   const handleShowLogs = () => {
  //     setShowLogs(!showLogs);
  //   };

  return (
    <React.Fragment>
      <tr>
        <td>{sitters.sitterName}</td>
        <td>{sitters.status}</td>
        <td>{sitters.description}</td>
        <td>
          <button
            className="btnfx-red"
            onClick={() => props.handleDeleteSitter(sitters._id)}
          >
            Delete
          </button>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default SitterRow;
