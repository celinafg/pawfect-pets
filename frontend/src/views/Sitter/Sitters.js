import React, { useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { SitterRow, SitterForm } from './components';
import axios from 'axios';
import Navbar from '../Layouts/Navbar';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const Sitters = () => {
	const [ sitters, setSitters ] = useState([]);
	const [ showForm, setShowForm ] = useState(false);
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');

	useEffect(() => {
		axios.get('http://localhost:4000/showsitters').then((res) => {
			setSitters(res.data);
		});
	}, []);

	const handleShowForm = () => {
		setShowForm(!showForm);
	};

	const handleNameInput = (e) => {
		setName(e.target.value);
	};

	const handleDescription = (e) => {
		setDescription(e.target.value);
	};

	const handleSaveSitter = () => {
		axios
			.post('http://localhost:4000/addsitter', {
				sitterName: name,
				description
			})
			.then((res) => {
				let newSitters = [ ...sitters ];
				newSitters.push(res.data);
				setSitters(newSitters);
				// handleRefresh();
			});
	};

	const handleDeleteSitter = (sitterId) => {
		axios.delete('http://localhost:4000/deletesitter/' + sitterId).then((res) => {
			let index = sitters.findIndex((sitter) => sitter._id === sitterId);
			let newSitters = [ ...sitters ];
			newSitters.splice(index, 1);
			setSitters(newSitters);
		});
	};

	return (
		<React.Fragment>
			<Navbar />
			<div>
				<div className="text-center">
					<h1 className="text-center py-5">Sitters</h1>
				</div>
				<div className="col-lg-10 offset-lg-1 ">
					<div className="py-2">
						<button className="btnfx-green" onClick={handleShowForm}>
							Add Sitter
						</button>
						<ReactHTMLTableToExcel
							id="sitterstable"
							className="btnfx ml-2"
							table="sitters"
							filename="sitters"
							sheet="sitters"
							buttonText="Export Sitters"
						/>
						<SitterForm
							showForm={showForm}
							handleShowForm={handleShowForm}
							handleNameInput={handleNameInput}
							handleDescription={handleDescription}
							handleSaveSitter={handleSaveSitter}
						/>
					</div>
					<table className="table table-striped border" id="sitters">
						<thead>
							<tr>
								<th>Sitter Name</th>
								<th>Status</th>
								<th>Description</th>
								<th>Actions</th>
							</tr>
						</thead>
						{sitters.map((sitters) => (
							<SitterRow key={sitters._id} sitters={sitters} handleDeleteSitter={handleDeleteSitter} />
						))}
						<tbody />
					</table>
				</div>
			</div>
		</React.Fragment>
	);
};

export default Sitters;
