import React, { useEffect, useState } from "react";
import Hero from "../Partials/Hero";
import Packages from "../Package/Packages";
import About from "../Partials/About";
import Quote from "../Partials/Quote";

const Home = () => {
  return (
    <React.Fragment>
      <Hero />
      <About />
      <Quote />
      <Packages />
    </React.Fragment>
  );
};
export default Home;
