import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {
	Navbar,
	NavbarBrand,
	NavbarToggler,
	Collapse,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownItem,
	DropdownMenu,
	DropdownToggle
} from 'reactstrap';

const NavBar = () => {
	const [ isOpen, setIsOpen ] = useState(false);
	const [ user, setUser ] = useState('');

	const toggle = () => setIsOpen(!isOpen);

	const imgStyle = {
		height: '8vh',
		width: 'auto',
		marginLeft: '12vh'
	};

	useEffect(() => {
		if (sessionStorage.token) {
			let user = JSON.parse(sessionStorage.user);
			setUser(user);
		}
	}, []);

	const handleLogout = () => {
		sessionStorage.clear();
		window.location.replace('/');
	};

	const link = {
		textDecoration: 'none'
		// color: "#3e6915",
		// fontWeight: "600 !important",
		// fontSize: "2.3vh",
		// letterSpacing: "0.8vh"
	};

	const transparent = {
		background: 'none !important'
	};

	return (
		<React.Fragment>
			<Navbar inverse fluid style={transparent} fixed="top" onScroll="light">
				<NavbarBrand>
					<div>
						<a style={link} href="#">
							<img src={require('./logo.png')} style={imgStyle} />

							<span className="pawfect">pawfect pets</span>
							{sessionStorage.token ? (
								<div class="upper colorgreen ml" style={link}>
									Hello {user.name}!
								</div>
							) : (
								''
							)}
						</a>
					</div>
				</NavbarBrand>

				<Nav pullRight className="mr-5 ">
					<div className="floatr">
						<Link style={link} to="/packages">
							Packages
						</Link>

						{user.isAdmin ? (
							<div className="floatr">
								<Link className="ml-2" style={link} to="/allbookings">
									Bookings
								</Link>
								<Link className="ml-2" style={link} to="/sitters">
									Sitters
								</Link>{' '}
							</div>
						) : (
							''
						)}

						{sessionStorage.token ? (
							''
						) : (
							<div className="floatr">
								<Link className="ml-2" style={link} to="/login">
									Login
								</Link>
								<Link className="ml-2" style={link} to="/register">
									Register
								</Link>
							</div>
						)}

						{sessionStorage.token ? (
							<Link className="ml-2" style={link} onClick={handleLogout}>
								Logout
							</Link>
						) : (
							''
						)}
					</div>
				</Nav>
			</Navbar>
			{/* <Navbar inverse fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#">Brand</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        <Nav pullRight>
          <NavItem eventKey={1} href="#">
            Hello
          </NavItem>
          <NavItem eventKey={2} href="#">
            World
          </NavItem>
        </Nav>
      </Navbar> */}

			{/* <nav class="navbar-transparent navbar-expand-lg" style={navbar}>
        <div class="collapse navbar-collapse d-flex" id="navbarNavAltMarkup">
          <div class="navbar-nav ">
            <div className="d-flex flex-row justify-content-center mt-2 align-items-baseline">
              <a class="" href="#">
                <img src={require("./logo.png")} style={imgStyle} />
              </a>
              <p className="pawfect">
                pawfect pets
                <br />{" "}
                <div class="upper colorgreen" style={link}>
                  Hello {user.name}
                </div>
              </p>

              <div className="floatr">
                <a class="nav-item nav-link" href="">
                  <Link style={link} to="/packages">
                    Packages
                  </Link>
                </a>
                <a class="nav-item nav-link" href="">
                  <Link style={link} to="/login">
                    LOGIN
                  </Link>
                </a>

                <a class="nav-item nav-link" href="" onClick={handleLogout}>
                  <Link style={link}>LOGOUT</Link>
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav> */}
		</React.Fragment>
	);
};
export default NavBar;
