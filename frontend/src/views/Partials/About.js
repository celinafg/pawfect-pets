import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';

const About = () => {
	const noBorder = {
		border: '1px solid white'
	};

	const background = {
		height: '100vh',
		width: '100vw'
	};

	const works = {
		paddingTop: '20vh',
		fontFamily: 'Montserrat',
		fontSize: '50vh !important'
	};

	const p = {
		fontSize: '3vh',
		color: '#9b9b9bfd'
	};

	const icon1 = {
		backgroundImage: 'url(' + require('../../images/box.png') + ')',
		height: '25px',
		width: '25px',
		backgroundSize: 'cover',
		display: 'inline-block',
		border: '0',
		marginRight: '8px'
	};

	const icon2 = {
		backgroundImage: 'url(' + require('../../images/calendar.png') + ')',
		height: '25px',
		width: '25px',
		backgroundSize: 'cover',
		display: 'inline-block',
		border: '0',
		marginRight: '8px'
	};
	const icon3 = {
		backgroundImage: 'url(' + require('../../images/card.png') + ')',
		height: '25px',
		width: '25px',
		backgroundSize: 'cover',
		display: 'inline-block',
		border: '0',
		marginRight: '8px'
	};

	const meow = {
		backgroundImage: 'url(' + require('../../images/cat.png') + ')',
		height: '10vh',
		width: '4.7vw',
		backgroundSize: 'contain',
		marginTop: '18vh',
		border: '0',
		zIndex: '500',
		opacity: '0.8'
	};

	return (
		<React.Fragment>
			<Container style={background}>
				<Row className="d-flex justify-content-center">
					<div style={meow} />
				</Row>
				<Row>
					<Col className="works d-flex justify-content-center">
						<h2>Here's How it Works</h2>
					</Col>
				</Row>

				<Row className="higher">
					<Col>
						<Card style={noBorder}>
							{/* <CardImg
                top
                width="auto"
                height="30vh"
                src="/assets/318x180.svg"
                alt="Choose a package"
              /> */}
							<CardBody>
								<CardSubtitle>
									<p className="upper">Step 1</p>
								</CardSubtitle>
								<CardTitle>
									<h3 className="header">
										<span style={icon1} />
										Choose A Package
									</h3>
								</CardTitle>
								<CardText>
									<p style={p}>
										Look through our offerings and find the best fit for your pet. We've got
										something for every pet!
									</p>
								</CardText>
							</CardBody>
						</Card>
					</Col>

					<Col>
						<Card style={noBorder}>
							<CardBody>
								<CardSubtitle>
									<p className="upper">Step 2</p>
								</CardSubtitle>
								<CardTitle>
									<h3 className="header">
										<span style={icon2} /> Day + Sitter
									</h3>
								</CardTitle>
								<CardText>
									<p style={p}>
										Choose your dates from our calendar and choose your sitter. Our community of
										vetted sitters are pet lovers at heart, and are excited to receive you and your
										pet!
									</p>
								</CardText>
							</CardBody>
						</Card>
					</Col>

					<Col>
						<Card style={noBorder}>
							{/* <CardImg
                top
                width="auto"
                height="30vh"
                src="/assets/318x180.svg"
                alt="Choose a package"
              /> */}
							<CardBody>
								<CardSubtitle>
									<p className="upper">Step 3</p>
								</CardSubtitle>
								<CardTitle>
									<h3 className="header">
										<span style={icon3} />Checkout Payment
									</h3>
								</CardTitle>
								<CardText>
									<p style={p}>
										This is the last and final step. After payment, you will receive a booking
										receipt. Don't forget to bring this to our shop on the day of drop off. Can't
										wait!
									</p>
								</CardText>
							</CardBody>
						</Card>
					</Col>
				</Row>
			</Container>
		</React.Fragment>
	);
};

export default About;
