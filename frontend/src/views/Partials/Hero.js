import React from "react";
import Navbar from "../Layouts/Navbar";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";

const Hero = () => {
  const background = {
    backgroundImage: "url(" + require("../../images/herolarge.jpeg") + ")",
    height: "100vh",
    width: "100%",
    backgroundSize: "cover",
    backgroundColor: "black",
    backgroundPosition: "bottom",
    marginLeft: "40px"
  };

  const formatCenter = {
    paddingLeft: "17vh",
    paddingTop: "23vh"
  };
  const link = {
    textDecoration: "none",
    color: "#3e6915",
    fontWeight: "600 !important",
    fontSize: "2.3vh",
    letterSpacing: "0.8vh"
  };
  const extrablue = {
    background: "#93c7ce"
  };

  return (
    <React.Fragment>
      <Navbar />
      <div style={extrablue} href="#packages">
        <div className="formatcenter" style={background}>
          <div style={formatCenter}>
            <h1>
              Your bestfriend <br />
              is in good hands!
            </h1>
            <p className="darkgray mt-3">
              Going away? Find the care your pet deserves! <br />
              Find a temporary home for your pets from <br />
              our community of pet lovers.
            </p>
            <Link to="/packages" className="btnfx" style={link}>
              See Packages!
            </Link>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Hero;
