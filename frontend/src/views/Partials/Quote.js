import React from "react";
import { Container, Row, Col } from "reactstrap";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from "reactstrap";

const Quote = () => {
  const background = {
    backgroundImage: "url(" + require("../../images/paws.jpg") + ")",
    backgroundSize: "cover",
    height: "80vh"
  };

  const contain = {
    border: "3px solid hsl(0, 70%, 62%);"
  };

  const bullet = {
    backgroundImage: "url(" + require("../../images/paw.png") + ")",
    backgroundSize: "cover",
    height: "100px",
    width: "100px",
    zIndex: "9000"
  };

  return (
    <React.Fragment>
      <div style={background}>
        <Container
          style={contain}
          className="col-lg-6 offset-lg-3 text-center d-flex justify-content-center"
        >
          <Row>
            <h2 class="header mt">
              If you’re looking for quality Pet Sitting, the Pawfect Pet has you
              covered.
            </h2>
            <h5 className="vetted mt-4">
              <span style={bullet}></span>Whether you’re planning an overseas
              trip, a weekend break, or an overnight getaway, your Pawfect Pets
              Sitter will care for your furry friend while you’re away.
            </h5>
            <h5 className="vetted mt-4">
              <span style={bullet}></span>With vetted, insured Pet Sitters all
              around the Philippines, Pawfect Pets is the paw-fect option for
              your pet.
            </h5>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default Quote;
