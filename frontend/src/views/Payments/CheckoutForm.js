import React, { useState } from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import axios from 'axios';
import moment from 'moment';
import swal from 'sweetalert';
import Receipt from './Receipt';

const CheckoutForm = (props) => {
	let totalPayment = props.totalPayment;
	let days = props.days;
	let customerName = props.customerName;
	let customerEmail = props.customerEmail;
	let sitterName = props.sitterName;
	let sitterId = props.sitterId;
	let packName = props.packName;
	let packId = props.packId;

	const submit = async (e) => {
		console.log(customerEmail);

		let { token } = await props.stripe.createToken({
			name: 'Name'
		});
		let tokenId = token.id;
		let amount = 30000;
		//

		let data = new FormData();
		data.append('amount', 12345);

		axios
			.post('https://boiling-temple-81418.herokuapp.com/charge', {
				amount: props.totalPayment * 100,
				source: token.id,
				receipt_email: customerEmail
			})
			.then((res) => {
				console.log(res);

				let bookingCode = moment(new Date()).format('YYYY-MM-DD');
				let startDate = moment(days.from).format('YYYY-MM-DD');
				let endDate = moment(days.to).format('YYYY-MM-DD');

				axios({
					method: 'POST',
					url: 'https://boiling-temple-81418.herokuapp.com/addbooking',
					data: {
						bookingCode: bookingCode,
						cost: totalPayment,

						name: customerName,
						schedule: [
							{
								startDate,
								endDate
							}
						],
						email: customerEmail,
						sitterName: sitterName,
						sitterId: sitterId,
						packName: packName,
						packId: packId
					}
				}).then((res) => {
					console.log(res.data);
					swal({
						title: "Your're All Set!",
						text: `Don't forget to print out the receipt before bringing your pup to the shop!`,
						className: 'red-bg',
						icon: 'success'
					});
				});
			});
	};

	return (
		<React.Fragment>
			<div
				className="checkout"
				hidden={
					props.customerName === '' || props.customerEmail === '' || props.totalPayment < 0 ? true : false
				}
			>
				<div className="forms mb-3">Please complete the purchase to confirm your booking!</div>
				<CardElement className="mb-4" />
				<button class="btnfx-green mb-1" onClick={(e) => submit(e)}>
					Pay Now!
				</button>
			</div>
		</React.Fragment>
	);
};

export default injectStripe(CheckoutForm);
