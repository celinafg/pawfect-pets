import React from "react";
import CheckoutForm from "./CheckoutForm";
import { StripeProvider, Elements } from "react-stripe-elements";
import Receipt from "./Receipt";

const Payments = props => {
  // let totalPayment = props.totalPayment;
  return (
    <React.Fragment>
      <StripeProvider apiKey="pk_test_fOTIIfFLq79gMNrqvOzo9wzS00ol3IKqa0">
        <div>
          {/* <h1 className="text-center py-5">Give me Money</h1> */}
          <div className="forms d-flex justify-content-center">
            <Elements>
              <CheckoutForm
                totalPayment={props.totalPayment}
                days={props.days}
                customerName={props.customerName}
                customerEmail={props.customerEmail}
                sitterName={props.sitterName}
                sitterId={props.sitterId}
                packName={props.packName}
                packId={props.packId}
              />
            </Elements>
          </div>
        </div>
      </StripeProvider>
    </React.Fragment>
  );
};

export default Payments;
