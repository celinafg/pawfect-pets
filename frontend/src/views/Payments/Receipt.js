import React, { useState, useEffect } from "react";

const Receipt = props => {
  //   const [dropdownOpen, setDropdownOpen] = useState(false);

  //   const toggle = () => {
  //     setDropdownOpen(!dropdownOpen);
  //   };

  return (
    <table>
      <thead>
        <th>Customer Name</th>
        <th>Customer Email</th>
        <th>Sitter Name</th>
        <th>Package Name</th>
        <th>Schedule</th>
        <th>Total Payment</th>
      </thead>
      <tbody>
        <td>hi</td>
        <td>{props.customerEmail}</td>
        <td>{props.sitterName}</td>
        <td>{props.packName}</td>
        <td>{props.days}</td>
      </tbody>
    </table>
  );
};

export default Receipt;
