import React, { useState } from 'react';
import { FormInput } from '../../components';
import { Button } from 'reactstrap';
import Axios from 'axios';

const base_url = 'https://boiling-temple-81418.herokuapp.com';

const Login = () => {
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ user, setUser ] = useState('');

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		console.log(e.target.value);
	};

	const handlePasswordChange = (e) => {
		setPassword(e.target.value);
		console.log(e.target.value);
	};

	const handleLogin = () => {
		Axios.post(base_url + '/login', {
			email,
			password
		}).then((res) => {
			sessionStorage.token = res.data.token;
			sessionStorage.user = JSON.stringify(res.data.user);
			window.location.replace('/');
			// setUser(sessionStorage.user);
			// console.log(user);
		});
	};

	return (
		<React.Fragment>
			<div class="contenedor">
				<div class="todo">
					<div class="dog">
						<span class="leg3" />
						<div class="body">
							<span class="cola" />
							<span class="leg" />
						</div>
						<div class="cabezota">
							<div class="orejas">
								<span class="orejitas" />
							</div>
							<div class="orejas3">
								<span class="orejitas3" />
							</div>
							<div class="cabeza">
								<span class="cabeza3" />
								<span class="ojos">
									<span class="iris" />
								</span>
								<span class="nariz" />
								<span class="nariz3" />
							</div>
						</div>

						<div class="canasta" />
					</div>
				</div>
			</div>
			<div className="red-bg2">
				<h1 className="py-5 col-lg-4 offset-lg-6 text-center">Login</h1>
				<div className="col-lg-4 offset-lg-6">
					<FormInput
						label={'Email'}
						placeholder={'Enter your email'}
						type={'email'}
						onChange={handleEmailChange}
					/>
					<FormInput
						label={'Password'}
						placeholder={'Enter your password'}
						type={'password'}
						onChange={handlePasswordChange}
					/>
					<button block className="btnfx-blue" onClick={handleLogin}>
						Login
					</button>
				</div>
			</div>

			{/*  */}
		</React.Fragment>
	);
};

export default Login;
