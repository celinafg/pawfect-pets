import React, { useState } from 'react';
import { FormInput } from '../../components';
import { Button } from 'reactstrap';
import swal from 'sweetalert';
import Axios from 'axios';

const base_url = 'https://boiling-temple-81418.herokuapp.com';

const Register = () => {
	const [ name, setName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const handleNameChange = (e) => {
		setName(e.target.value);
		console.log(e.target.value);
	};

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		console.log(e.target.value);
	};

	const handlePasswordChange = (e) => {
		setPassword(e.target.value);
		console.log(e.target.value);
	};

	const handleRegister = () => {
		Axios.post(base_url + '/register', {
			name,
			email,
			password
		}).then((res) => {
			swal({
				title: 'Succesfully Registered!',
				text: 'thanks for joining us :)',
				className: 'red-bg',
				icon: 'success'
			});
			window.location.replace('#/login');
		});
	};

	return (
		<React.Fragment>
			<div className="red-bg2">
				<div class="container ">
					<div className="col">
						<div class="dog">
							<div class="dog-head">
								<div class="dog-ears ears-left" />
								<div class="dog-ears ears-right" />
								<div class="dog-eyes" />
								<div class="dog-mouth">
									<div class="dog-nose" />
									<div class="dog-tongue" />
								</div>
							</div>
							<div class="dog-tail" />
							<div class="dog-body">
								<div class="dog-foot" />
							</div>
							<div class="ball" />
						</div>
					</div>
				</div>
				<div className="container">
					<div className="col ">
						<div className="col-lg-8 offset-lg-2">
							<h1 className="text-center py-5 reg">Register</h1>
							<div className="col-lg-8 offset-lg-2">
								<FormInput
									label={'Name'}
									placeholder={'Enter your name'}
									type={'text'}
									onChange={handleNameChange}
								/>
								<FormInput
									label={'Email'}
									placeholder={'Enter your email'}
									type={'email'}
									onChange={handleEmailChange}
								/>
								<FormInput
									label={'Password'}
									placeholder={'Enter your password'}
									type={'password'}
									onChange={handlePasswordChange}
								/>
								<button className="btnfx-blue" onClick={handleRegister}>
									Register
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default Register;
