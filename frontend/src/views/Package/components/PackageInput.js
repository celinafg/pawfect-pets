import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FormGroup, Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

const PackageInput = (props) => {
	const [ dropdownOpen, setDropdownOpen ] = useState(false);

	const toggle = () => {
		setDropdownOpen(!dropdownOpen);
	};

	const dp = {
		color: 'white',
		background: '#3e6915',
		fontFamily: 'Montserrat'
	};

	const [ packs, setPacks ] = useState([]);

	useEffect(() => {
		axios.get('https://boiling-temple-81418.herokuapp.com/showpackages').then((res) => {
			setPacks(res.data);
		});
	}, []);

	return (
		<FormGroup>
			{/* <Label>Package Name</Label> */}
			<Dropdown isOpen={dropdownOpen} toggle={toggle}>
				<DropdownToggle caret style={dp}>
					{props.packNameDrop == '' ? 'Choose Package' : props.packNameDrop}
				</DropdownToggle>
				<DropdownMenu size="sm">
					{packs.map((pack) => (
						<DropdownItem
							key={pack._id}
							onClick={() => {
								props.handlePackInput(pack);
							}}
						>
							<p className="red">{pack.name}</p>
						</DropdownItem>
					))}
				</DropdownMenu>
			</Dropdown>
		</FormGroup>
	);
};

export default PackageInput;
