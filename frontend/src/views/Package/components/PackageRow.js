import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'reactstrap';

import { Card, CardImg, CardGroup, CardText, CardBody, CardTitle, CardFooter, CardSubtitle, Button } from 'reactstrap';
import { userInfo } from 'os';
// import { FormInput } from "../../../components";

const PackageRow = ({ packageservice, ...props }) => {
	let user = props.user;
	const p = {
		fontSize: '3vh',
		color: '#9b9b9bfd'
	};

	const padd = {
		height: '40px'
	};

	return (
		<React.Fragment>
			{/* <CardGroup>
				<Card>
					<CardImg top width="100%" src="/assets/256x186.svg" alt="Card image cap" />
					<CardBody>
						<CardTitle>Card title</CardTitle>
						<CardSubtitle>Card subtitle</CardSubtitle>
						<CardText>
							This is a wider card with supporting text below as a natural lead-in to additional content.
							This content is a little bit longer.
						</CardText>
						<Button>Button</Button>
					</CardBody>
				</Card>
			</CardGroup> */}
			<Col>
				<Card className="fixedwh">
					<CardBody>
						<CardTitle>
							<h3 className="header2">{packageservice.name}</h3>
						</CardTitle>
						<CardText>
							<p style={p}>{packageservice.description}</p>
						</CardText>
						<CardSubtitle className="d-flex justify-content-end mt-4 cost">
							₱{packageservice.cost}
						</CardSubtitle>
					</CardBody>
					<CardText className="none" style={padd}>
						{user.isAdmin ? (
							<button
								className="btnfx-red mb-4"
								onClick={() => props.handleDeletePackage(packageservice._id)}
							>
								Delete
							</button>
						) : (
							''
						)}
					</CardText>
				</Card>
			</Col>

			{/* <tr> */}
			{/* <td>{packageservice.code}</td>
				<td>{packageservice.name}</td>
				<td>{packageservice.description}</td>
				<td>{packageservice.cost}</td>
				<td>
					<Button color="danger" onClick={() => props.handleDeletePackage(packageservice._id)}>
						Delete
					</Button>
				</td> */}
			{/* </tr> */}

			{/* trial */}
			{/* <div class="container d-flex flex-column">
				<div class="row">
					<div class="col-sm-4">
						<div class="card card-flip text-white bg-dark">
							<div class="card-front ">
								<div class="card-body">
									<i class="fa fa-search fa-5x float-right" />
									<h3 class="card-title">{packageservice.name}</h3>
									<p class="card-text">{packageservice.description}</p>
								</div>
							</div>
							<div class="card-back">
								<div class="card-body">
									<h3 class="card-title">Back</h3>
									<p class="card-text">{packageservice.cost}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> */}
		</React.Fragment>
	);
};

export default PackageRow;
