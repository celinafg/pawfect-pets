import React, { useState, useEffect } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment';
import { SitterInput } from '../components';
import axios from 'axios';
import moment from 'moment';

const DayInput = (props) => {
	const [ sitterName, setSitterName ] = useState('');
	const [ sitterId, setSitterId ] = useState('');
	const [ sitterNameDrop, setSitterNameDrop ] = useState('');
	const [ noDays, setDisabledDays ] = useState([]);
	const [ startDate, setStartDate ] = useState('');
	const [ endDate, setEndDate ] = useState('');

	let toInput = React.createRef();
	function showFromMonth() {
		const { from, to } = props.days;
		if (!from) {
			return;
		}
	}

	useEffect(
		() => {
			showFromMonth();
		},
		[ props.days.to, showFromMonth ]
	);

	const { from, to } = props.days;

	const modifiers = {
		start: from,
		end: to
	};

	const handleSitterInput = (sitter) => {
		setSitterName(sitter.sitterName);
		setSitterId(sitter._id);
		setSitterNameDrop(sitter.sitterName);
		props.sendFunction(sitterName, sitterId, sitterNameDrop);

		axios.get('https://boiling-temple-81418.herokuapp.com/showbookingbysitterid/' + sitterId).then((res) => {
			res.data.forEach((sitter) => {
				setDisabledDays([ ...noDays, sitter.schedule ]);
				setStartDate(sitter.schedule[0].startDate);
				setEndDate(sitter.schedule[0].endDate);
			});
		});
	};

	const past = {
		before: new Date()
	};

	const disabled = {
		from: new Date(startDate),
		to: new Date(endDate)
	};

	return (
		<div className="InputFromTo">
			<div className="text-center">
				<SitterInput
					handleSitterInput={handleSitterInput}
					sitterName={props.sitterName}
					sitterNameDrop={props.sitterNameDrop}
				/>
			</div>
			<div className="forms">Choose your dates:</div>

			<DayPickerInput
				value={from}
				formatDate={formatDate}
				parseDate={parseDate}
				placeholder="From"
				format="YYYY/M/D"
				dayPickerProps={{
					utc: true,
					selectedDays: [ from, { from, to } ],
					toMonth: to,
					month: to,
					disabledDays: [ disabled, past ],
					modifiers,
					numberOfMonths: 2,
					onDayClick: () => toInput.getInput().focus()
				}}
				onDayChange={props.handleFromChange}
			/>
			<span className="InputFromTo-to">
				<DayPickerInput
					ref={(el) => {
						toInput = el; //assign ref variable here
					}}
					value={to}
					placeholder="To"
					format="YYYY/M/D"
					formatDate={formatDate}
					parseDate={parseDate}
					dayPickerProps={{
						selectedDays: [ from, { from, to } ],
						disabledDays: [ disabled, past ],
						month: from,
						fromMonth: from,
						numberOfMonths: 2,
						utc: true
					}}
					onDayChange={props.handleToChange}
				/>
			</span>
		</div>
	);
};

export default DayInput;
