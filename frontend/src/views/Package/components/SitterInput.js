import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FormGroup, Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

const SitterInput = (props) => {
	const [ dropdownOpen, setDropdownOpen ] = useState(false);

	const toggle = () => {
		setDropdownOpen(!dropdownOpen);
	};
	const dp = {
		color: 'white',
		background: '#3e6915',
		fontFamily: 'Montserrat'
	};

	const [ sitters, setSitters ] = useState([]);

	useEffect(() => {
		axios.get('https://boiling-temple-81418.herokuapp.com/showsitters').then((res) => {
			setSitters(res.data);
		});
	}, []);

	return (
		<FormGroup>
			{/* <Label>Sitter Name</Label> */}
			<Dropdown autofocus isOpen={dropdownOpen} toggle={toggle}>
				<DropdownToggle autofocus caret style={dp}>
					{props.sitterNameDrop == '' ? 'Choose Sitter' : props.sitterNameDrop}
				</DropdownToggle>
				<DropdownMenu autofocus>
					{sitters.map((sitter) => (
						<DropdownItem autofocus key={sitter._id} onClick={() => props.handleSitterInput(sitter)}>
							{sitter.sitterName}
						</DropdownItem>
					))}
				</DropdownMenu>
			</Dropdown>
		</FormGroup>
	);
};

export default SitterInput;
