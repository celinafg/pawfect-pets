import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { Modal, ModalBody, ModalHeader, Button } from 'reactstrap';
import { FormInput } from '../../../components';
import { SitterInput, PackageInput, DayInput } from '../components';

import Payments from '../../Payments/Payments';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const BookingForm = (props) => {
	let user = props.user;
	const bg = {
		backgroundImage: 'url(' + require('../../../images/paws.jpg') + ')',
		backgroundSize: 'cover',
		display: 'inline',
		border: '10px solid white',
		height: '14vh'
	};

	return (
		<React.Fragment>
			<Modal isOpen={props.showBookForm} toggle={props.handleShowBookForm}>
				<ModalHeader toggle={props.handleShowBookForm} style={bg}>
					<h3 className="booking text-center">Book Now!</h3>
				</ModalHeader>

				<ModalBody className="container col-sm-11">
					<div className="d-flex justify-content-around">
						<PackageInput
							handlePackInput={props.handlePackInput}
							packName={props.packName}
							packNameDrop={props.packNameDrop}
							required
						/>

						{/* <SitterInput
						// // handleSitterInput={props.handleSitterInput}
						// sitterName={props.sitterName}
						// sitterNameDrop={props.sitterNameDrop}
						/> */}
					</div>

					<DayInput
						handleFromChange={props.handleFromChange}
						handleToChange={props.handleToChange}
						days={props.days}
						handleDisabled={props.handleDisabled}
						sitterName={props.sitterName}
						sitterNameDrop={props.sitterNameDrop}
						sendFunction={props.sendFunction}
						days={props.days}
					/>
				</ModalBody>

				<div className="col-sm-10 offset-sm-1">
					<FormInput
						className="forms"
						label={'Your Name'}
						type={'text'}
						name={'name'}
						placeholder="First, Last"
						onChange={(e) => [ props.handleCustomerNameInput(e) ]}
					/>
					<FormInput
						className="forms"
						label={'Your Email'}
						type={'email'}
						name={'email'}
						placeholder="name@name.com"
						onChange={(e) => [ props.handleCustomerEmailInput(e) ]}
					/>
					<div className="d-flex flex-row justify-content-between">
						<div className="forms mt-2 size">Cost: Php {props.totalPayment}</div>

						<button className="btnfx pull-right py-2" onClick={props.handleCalculateCost}>
							Calculate
						</button>
					</div>
					<div className="py-5 text-center">
						<Payments
							totalPayment={props.totalPayment}
							days={props.days}
							customerName={props.customerName}
							customerEmail={props.customerEmail}
							sitterName={props.sitterName}
							sitterId={props.sitterId}
							packName={props.packName}
							packId={props.packId}
						/>
					</div>
				</div>
				<Helmet>
					<style>{`
			.Selectable .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
				background-color: #f0f8ff !important;
				color: #4a90e2;
			}
			.Selectable .DayPicker-Day {
				border-radius: 0 !important;
			}
			.Selectable .DayPicker-Day--start {
				border-top-left-radius: 50% !important;
				border-bottom-left-radius: 50% !important;
			}
			.Selectable .DayPicker-Day--end {
				border-top-right-radius: 50% !important;
				border-bottom-right-radius: 50% !important;
			}
			`}</style>
				</Helmet>
			</Modal>
		</React.Fragment>
	);
};

export default BookingForm;

// class BookingForm extends Component {
// 	state = {
// 		from: undefined,
// 		to: undefined
// 	};

// handleDayClick = (day) => {
// 	const range = DateUtils.addDayToRange(day, this.state);
// 	this.setState(range);
// };

// handleResetClick = () => {
// 	this.setState({ from: undefined, to: undefined });
// };

// render() {
// const { from, to } = this.props.state;
// const modifiers = { start: from, end: to };
// console.log(from, to);

// return (
// <React.Fragment>
// 	<Modal isOpen={this.props.showBookForm} toggle={this.props.handleShowBookForm}>
// 		<ModalHeader
// 			toggle={this.props.handleShowBookForm}
// 			style={{ backgroundColor: 'lightsalmon', color: 'white' }}
// 		>
// 			Book Now! */}
// 		</ModalHeader>
// 		<ModalBody>
// 			<PackageInput
// 				handlePackInput={this.props.handlePackInput}
// 				packName={this.props.packName}
// 				packNameDrop={this.props.packNameDrop}
// 			/>
// 			<SitterInput
// 				handleSitterInput={this.props.handleSitterInput}
// 				sitterName={this.props.sitterName}
// 				sitterNameDrop={this.props.sitterNameDrop}
// 			/>
// 		</ModalBody>
// 		<FormInput
// 			label={'Your Name'}
// 			type={'text'}
// 			name={'name'}
// 			placeholder="Your Name"
// 			onChange={(e) => [ this.props.handleCustomerNameInput(e) ]}
// 		/>
// 		<FormInput
// 			label={'Your Email'}
// 			type={'email'}
// 			name={'email'}
// 			placeholder="Your Email"
// 			onChange={(e) => [ this.props.handleCustomerEmailInput(e) ]}
// 		/>

// 		<div className="RangeExample">
// 			<p>
// 				{!from && !to && 'Please select the first day.'}
// 				{from && !to && 'Please select the last day.'}
// 				{from &&
// 					to &&
// 					`Selected from ${from.toLocaleDateString()} to
//     ${to.toLocaleDateString()}`}{' '}
// 				{from &&
// 				to && (
// 					<button className="link" onClick={this.props.handleResetClick}>
// 						Reset
// 					</button>
// 				)}
// 			</p>
// 			<DayPicker
// 				className="Selectable"
// 				numberOfMonths={this.props.numberOfMonths}
// 				selectedDays={[ from, { from, to } ]}
// 				modifiers={modifiers}
// 				onDayClick={this.props.handleDayClick}
// 			/>
// 		</div>
// 		<h3>Cost</h3>
// 		<Button onClick={this.props.handleCalculateCost}>Calculate</Button>
// 		<Button
// 			color="info"
// 			block
// 			// disabled={props.assetName === '' || props.quantity <= 0 ? true : false}
// 			// onClick={props.handlePackageSaveRequest}
// 		>
// 			Book Sitter
// 		</Button>
// 		<Helmet>
// 			<style>{`
// .Selectable .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
// 	background-color: #f0f8ff !important;
// 	color: #4a90e2;
// }
// .Selectable .DayPicker-Day {
// 	border-radius: 0 !important;
// }
// .Selectable .DayPicker-Day--start {
// 	border-top-left-radius: 50% !important;
// 	border-bottom-left-radius: 50% !important;
// }
// .Selectable .DayPicker-Day--end {
// 	border-top-right-radius: 50% !important;
// 	border-bottom-right-radius: 50% !important;
// }
// `}</style>
// 		</Helmet>
// 	</Modal>
// </React.Fragment>
// 		);
// 	}
// }
