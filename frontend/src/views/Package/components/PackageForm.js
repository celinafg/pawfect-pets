import React from 'react';
import { Modal, ModalBody, ModalHeader, Button } from 'reactstrap';
import { FormInput } from '../../../components';

const PackageForm = (props) => {
	const bg = {
		backgroundImage: 'url(' + require('../../../images/paws.jpg') + ')',
		backgroundSize: 'cover',
		display: 'inline',
		border: '10px solid white',
		height: '14vh'
	};

	return (
		<React.Fragment>
			<Modal isOpen={props.showForm} toggle={props.handleShowForm}>
				<ModalHeader toggle={props.handleShowForm} style={bg}>
					<h3 className="text-center booking"> Add Package </h3>
				</ModalHeader>
				<ModalBody className="container col-sm-11">
					{/* <AssetInput handleAssetInput={props.handleAssetInput} assetName={props.assetName} /> */}

					<FormInput
						label={'Package Name'}
						type={'text'}
						name={'name'}
						placeholder="Package Name"
						onChange={(e) => [ props.handleNameInput(e) ]}
					/>
					<FormInput
						label={'Package Description'}
						type={'text'}
						name={'description'}
						placeholder="Short description here"
						onChange={(e) => [ props.handleDescriptionInput(e) ]}
					/>
					<FormInput
						label={'Cost'}
						type={'number'}
						name={'cost'}
						onChange={(e) => [ props.handleCostInput(e) ]}
						placeholder="100"
						defaultValue={1}
					/>
					<button
						className="btnfx-green"
						block
						// disabled={props.assetName === '' || props.quantity <= 0 ? true : false}
						onClick={props.handlePackageSaveRequest}
					>
						Add Package
					</button>
				</ModalBody>
			</Modal>
		</React.Fragment>
	);
};

export default PackageForm;
