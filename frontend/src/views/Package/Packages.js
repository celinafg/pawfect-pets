import React, { useEffect, useState } from 'react';
import { Row, CardDeck } from 'reactstrap';
import moment from 'moment';
import { PackageRow, PackageForm, BookingForm } from './components';
import axios from 'axios';
import Navbar from '../Layouts/Navbar';
import Receipt from '../Payments/Receipt';

const Packages = (props) => {
	const [ packs, setPacks ] = useState([]);
	const [ showForm, setShowForm ] = useState(false);
	const [ showBookForm, setShowBookForm ] = useState(false);
	const [ cost, setCost ] = useState(1);
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ packName, setPackName ] = useState('');
	const [ packId, setPackId ] = useState('');
	const [ sitterName, setSitterName ] = useState('');
	const [ sitterId, setSitterId ] = useState('');
	const [ packNameDrop, setPackNameDrop ] = useState('');
	const [ packCost, setPackCost ] = useState('');
	const [ sitterNameDrop, setSitterNameDrop ] = useState('');
	const [ customerName, setCustomerName ] = useState('');
	const [ customerEmail, setCustomerEmail ] = useState('');
	const [ days, setDays ] = useState({
		from: undefined,
		to: undefined
	});
	const [ totalPayment, setTotalPayment ] = useState();
	const [ bookings, setBookings ] = useState('');
	const [ user, setUser ] = useState({});

	const sendFunction = (sitterName, sitterId, sitterNameDrop) => {
		setSitterName(sitterName);
		setSitterId(sitterId);
		setSitterNameDrop(sitterNameDrop);
	};

	//dhdhdhd
	const handleFromChange = (from) => {
		// Change the from date and focus the "to" input field
		//This is functional setState which will only update `from` value
		setDays((days) => ({
			...days,
			from
		}));
	};

	const handleToChange = (to) => {
		//This is functional setState which will only update `to` value
		setDays((days) => ({
			...days,
			to
		}));
	};
	//ddhdhdh

	useEffect(() => {
		axios.get('https://boiling-temple-81418.herokuapp.com/showpackages').then((res) => {
			setPacks(res.data);
		});
	}, []);

	useEffect(() => {
		if (sessionStorage.token) {
			fetch('https://boiling-temple-81418.herokuapp.com/showpackages').then((response) => response.json());
			// .then((data) => setPackages(data));
			let user = JSON.parse(sessionStorage.user);
			setUser(user);
		} else {
			window.location.replace('#/');
		}
	}, []);

	const handleShowForm = () => {
		setShowForm(!showForm);
	};

	const handleShowBookForm = () => {
		setShowBookForm(!showBookForm);
	};

	const handleNameInput = (e) => {
		setName(e.target.value);
	};

	const handleDescriptionInput = (e) => {
		setDescription(e.target.value);
	};

	const handleCostInput = (e) => {
		setCost(e.target.value);
	};

	const handlePackageSaveRequest = () => {
		let code = moment(new Date()).format('x');

		axios
			.post('https://boiling-temple-81418.herokuapp.com/addpackage', {
				code: code,
				name,
				description,
				cost
			})
			.then((res) => {
				let newPacks = [ ...packs ];
				newPacks.push(res.data);
				setPacks(newPacks);
				// handleRefresh();
			});
	};

	const handleDeletePackage = (packageId) => {
		axios.delete('https://boiling-temple-81418.herokuapp.com/deletepackage/' + packageId).then((res) => {
			let index = packs.findIndex((pack) => pack._id === packageId);
			let newPacks = [ ...packs ];
			newPacks.splice(index, 1);
			setPacks(newPacks);
		});
	};

	const handlePackInput = (pack) => {
		setPackName(pack.name);
		setPackId(pack._id);
		setPackNameDrop(pack.name);
		setPackCost(pack.cost);
		console.log(packName);
		console.log(packCost);
	};

	const handleCustomerNameInput = (e) => {
		setCustomerName(e.target.value);
		console.log(e.target.value);
	};

	const handleCustomerEmailInput = (e) => {
		setCustomerEmail(e.target.value);
	};

	const handleCalculateCost = () => {
		let oneDay = 24 * 60 * 60 * 1000;
		let firstDate = new Date(days.from);
		let secondDate = new Date(days.to);
		let diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));

		let totalPayment = parseInt(diffDays) * parseInt(packCost);
		setTotalPayment(totalPayment);
	};

	return (
		<React.Fragment>
			<Navbar />
			<div className="background">
				<div className="text-center">
					<h2 className="packages text-center py-5">Services</h2>
				</div>
				<div className="col-lg-10 offset-lg-1">
					{/* Packages Form Modal */}
					{user.isAdmin ? (
						<button color="success" className="my-1 btnfx-green" onClick={handleShowForm}>
							Add Package
						</button>
					) : (
						''
					)}

					<PackageForm
						showForm={showForm}
						handleShowForm={handleShowForm}
						handleNameInput={handleNameInput}
						handleDescriptionInput={handleDescriptionInput}
						handleCostInput={handleCostInput}
						handlePackageSaveRequest={handlePackageSaveRequest}
					/>

					{/* <table className="table table-striped border">
						<thead>
							<tr>
								<th>Package Code</th>
								<th>Package Name</th>
								<th>Description</th>
								<th>Cost</th>
								<th>Actions</th>
							</tr>
						</thead> */}
					{/* <h3>{totalPayment}</h3> */}
					<Row className="d-flex flex-wrap">
						<CardDeck>
							{packs.map((packages) => (
								<PackageRow
									key={packages._id}
									packageservice={packages}
									handleDeletePackage={handleDeletePackage}
									handleShowBookForm={handleShowBookForm}
									showBookForm={showBookForm}
									handlePackInput={handlePackInput}
									packName={packName}
									user={user}
								/>
							))}
						</CardDeck>
					</Row>

					{/* <tbody /> */}
					<div className="d-flex justify-content-center py-3">
						{user.isAdmin ? (
							''
						) : (
							<button className="btnfx-blue" onClick={handleShowBookForm}>
								Book Now
							</button>
						)}
					</div>
					<BookingForm
						totalPayment={totalPayment}
						showBookForm={showBookForm}
						handleShowBookForm={handleShowBookForm}
						handlePackInput={handlePackInput}
						packName={packName}
						packId={packId}
						sitterName={sitterName}
						packNameDrop={packNameDrop}
						sitterNameDrop={sitterNameDrop}
						handleCustomerNameInput={handleCustomerNameInput}
						handleCustomerEmailInput={handleCustomerEmailInput}
						handleCalculateCost={handleCalculateCost}
						handleFromChange={handleFromChange}
						handleToChange={handleToChange}
						days={days}
						customerName={customerName}
						// handleSaveBooking={handleSaveBooking}
						sendFunction={sendFunction}
						customerEmail={customerEmail}
						sitterId={sitterId}
					/>
					{/* </table> */}
				</div>
			</div>
		</React.Fragment>
	);
};

export default Packages;
