import React, { useState, useEffect } from 'react';
import Navbar from '../Layouts/Navbar';
import { Button } from 'reactstrap';
import axios from 'axios';
import BookingRow from './BookingRow';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

const AllBookings = (props) => {
	const [ bookings, setBookings ] = useState([]);

	useEffect(() => {
		axios.get('https://boiling-temple-81418.herokuapp.com/showbookings').then((res) => {
			setBookings(res.data);
		});
	}, []);

	const handleDeleteBooking = (bookingId) => {
		axios.delete('https://boiling-temple-81418.herokuapp.com/deletebooking/' + bookingId).then((res) => {
			let index = bookings.findIndex((booking) => booking._id === bookingId);
			let newBookings = [ ...bookings ];
			newBookings.splice(index, 1);
			setBookings(newBookings);
		});
	};

	return (
		<React.Fragment>
			<Navbar />
			<div className="text-center">
				<h1 className="text-center py-5">Bookings</h1>
				{/* <Button
					color="success"
					// onClick={handleShowForm}
					className="my-3"
				>
					Add Request
				</Button> */}
			</div>

			<div className="col-lg-10 offset-lg-1">
				<div className="py-2">
					<ReactHTMLTableToExcel
						id="bookingstable"
						className="btnfx"
						table="bookings"
						filename="bookings"
						sheet="bookings"
						buttonText="Export Bookings"
					/>
				</div>
				<table className="table table-striped border" id="bookings">
					<thead>
						<tr>
							<th>Booking Code</th>
							<th>Customer Name</th>
							<th>Customer Email</th>
							<th>Sitter Name</th>
							<th>Package Name</th>
							<th>Cost</th>

							<th />
						</tr>
					</thead>
					<tbody>
						{bookings.map((booking) => (
							<BookingRow
								key={booking._id}
								booking={booking}
								handleDeleteBooking={handleDeleteBooking}
								days={props.days}
							/>
						))}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
};

export default AllBookings;
