import React, { useState } from 'react';
import { Container, Row, Col } from 'reactstrap';
import axios from 'axios';

const BookingRow = ({ booking, ...props }) => {
	const [ startDate, setStartDate ] = useState('');
	const [ endDate, setEndDate ] = useState('');
	const [ noDays, setDisabledDays ] = useState('');

	axios.get('https://boiling-temple-81418.herokuapp.com/showbookings/').then((res) => {
		res.data.forEach((sitter) => {
			setStartDate(sitter.schedule[0].startDate);
			setEndDate(sitter.schedule[0].endDate);
		});
	});

	return (
		<React.Fragment>
			<tr>
				<td>{booking.bookingCode}</td>
				<td>{booking.name}</td>
				<td>{booking.email}</td>
				<td>{booking.sitterName}</td>
				<td>{booking.packName}</td>
				<td>{booking.cost}</td>

				{/* {booking.schedule.map((schedule) => {
					<td key={schedule._id}>{schedule.startDate}</td>;
				})} */}
				{/* <td>{booking.schedule.startDate}</td>
				<td>{booking.endDate}</td> */}
				<td>
					<button className="btnfx-red" onClick={() => props.handleDeleteBooking(booking._id)}>
						Delete
					</button>
				</td>
			</tr>
		</React.Fragment>
	);
};

export default BookingRow;
